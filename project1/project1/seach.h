﻿#pragma once
#include <iostream>
#include <string>
#include <vector>

using namespace std;

//hàm tính toán bảng NEXT của chuỗi p
void initNextKMP(string p, vector<int> &next);

//hàm tìm kiếm chuỗi p trong chuỗi s bằng thuật toán Brute Force
//hàm trả về vị trí tìm thấy chuỗi p trong s
int seachBF(string s, string p);

//hàm tìm kiếm chuỗi p trong chuỗi s
//pos là vị trí tìm thấy chuỗi p trong s, nếu không tìm thấy pos = -1
//giá trị trả về kiểu bool, true khi tìm thấy hoặc false khi không tìm thấy
bool seachKMP(string s, string p, int& pos);

//hàm tìm kiếm chuỗi p trong chuỗi s
//pos là vị trí tìm thấy chuỗi p trong s, nếu không tìm thấy p = -1
//k là số lần so sánh chi tiết chuỗi p trong s khi giá trị hash trùng nhau
//giá trị trả về kiểu bool, true khi tìm thấy hoặc false khi không tìm thấy
bool seachRBK(string s, string p, int& pos, int& k);

