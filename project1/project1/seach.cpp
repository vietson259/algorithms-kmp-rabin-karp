﻿#pragma once

#include "seach.h"

//hàm tính toán bảng NEXT của chuỗi p
void initNextKMP(string p, vector<int> &next) {
	int i, j;
	int p_length = p.length();
	//bảng next có kích thước bằng chiều dài chuỗi p
	next.resize(p_length);
	//khởi tạo giá trị i, j và next[0] = -1;
	i = 0;
	j = next[0] = -1;
	while (i < p_length - 1) {
		if (j == -1 || p[i] == p[j]) {
			i++; j++;
			if (p[i] != p[j]) {
				next[i] = j;
			}
			else {
				next[i] = next[j];
			}
		}
		else j = next[j];
	}
}

//hàm tìm kiếm chuỗi p trong chuỗi s bằng thuật toán Brute Force
//hàm trả về vị trí tìm thấy chuỗi p trong s
int seachBF(string s, string p) {
	int s_length = s.length();
	int p_length = p.length();
	if (p_length == 0)
		return -1;
	for (int i = 0, j = 0; i <= s_length - p_length; i++) {
		while (s[i + j] == p[j]) {
			j++;
			if (j == p_length)
				return i;
		}
		j = 0;
	}
	return -1;
}

//hàm tìm kiếm chuỗi p trong chuỗi s
//pos là vị trí tìm thấy chuỗi p trong s, nếu không tìm thấy pos = -1
//giá trị trả về kiểu bool, true khi tìm thấy hoặc false khi không tìm thấy
bool seachKMP(string s, string p, int& pos) {
	pos = -1;
	int s_length = s.length();
	int p_length = p.length();
	//nếu chuỗi p rỗng (không có ký tự) thì trả về false
	if (p_length == 0)
		return false;
	//nếu chuỗi p chỉ có 1 ký tự ta tìm kiếm tuần tự bằng thuật toán Brute Force
	if (p_length == 1)
		return seachBF(s, p);
	//khỏi tạo bảng NEXT cho chuỗi p
	vector<int> NEXT;
	initNextKMP(p, NEXT);
	//bắt đầu tìm kiếm
	for (int i = 0, j = 0; i <= s_length - p_length; i++) {
		while (s[i + j] == p[j]) {
			j++;
			//nếu tìm thấy chuỗi p trong s
			if (j == p_length) {
				pos = i;
				return true;
			}
		}
		//nếu xảy ra mismatch, cập nhật lại i, j
		i = i + j - NEXT[j] - 1;
		if (j > 0)
			j = NEXT[j];
		if (j < 0)
			j = 0;
	}
	// trả về false vì không tìm thấy
	return false;
}


//hàm tìm kiếm chuỗi p trong chuỗi s
//pos là vị trí tìm thấy chuỗi p trong s, nếu không tìm thấy p = -1
//k là số lần so sánh chi tiết chuỗi p trong s khi giá trị hash trùng nhau
//giá trị trả về kiểu bool, true khi tìm thấy hoặc false khi không tìm thấy
bool seachRBK(string s, string p, int& pos, int& k) {
	//Khởi tạo giá trị mặc định cho pos và k
	pos = -1;
	k = 0;
	int s_length = s.length(), p_length = p.length();
	//nếu chuỗi con dài hơn chuỗi tìm kiếm thì trả về false
	if (p_length > s_length)
		return false;
	//nếu chuỗi con là rỗng (không có ký tự) thì trả về false
	if (p.length() == 0)
		return false;
	//tính giá trị hash cho chuỗi p
	int p_hash = 0;
	for (int i = 0, length = p.length(); i < length; i++) {
		p_hash += p[i];
	}
	//tính giá trị hash cho chuỗi s từ vị trí 0 có độ dài bằng chuỗi p
	int s_hash = 0;
	for (int i = 0; i < p_length; i++) {
		s_hash += s[i];
	}
	//bắt đầu tìm kiếm từ vị trí 0 của chuỗi s

	for (int i = 0; i <= s_length - p_length; i++) {
		//nếu giá trị hash trùng nhau
		if (s_hash == p_hash) {
			string src = s.substr(i, p_length);
			for (int j = 0; j < p_length; j++) {
				if (src[j] == p[j]) {
					k++;
				}
				else {
					k++;
					break;
				}
				if (j == p_length - 1) {
					pos = i;
					return true;
				}
			}
		}
		//tính toán lại giá trị hash của chuỗi s tiếp theo
		s_hash = s_hash - s[i] + s[i + p_length];
	}
	return false;
}

