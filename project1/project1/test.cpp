#pragma once

#include "test.h"

void taoChuoiNgauNhien(string tenFile, int chieuDai) {
	srand(time(0));
	ofstream file;
	file.open(tenFile);
	for (int i = 0; i < chieuDai; i++) {
		int temp = rand() % 2;
		file << temp;
	}
	file.close();
}

void taoChuoiNgauNhien1(string tenFile, int chieuDai) {
	srand(time(0));
	ofstream file;
	file.open(tenFile);
	for (int i = 0; i < chieuDai; i++) {
		int temp = (rand() % 10 + 24) % 3;
		file << temp;
	}
	file.close();
}

void docChuoiTuFile(string tenFile, string& chuoi) {
	ifstream docFile;
	docFile.open(tenFile);
	docFile >> chuoi;
	docFile.close();
}

