#include "seach.h"
#include "docGhiFile.h"

int main(int argc, char* argv[]) {
	vector<string> duLieu, chuoiDeTim;
	docFile(argv[1], duLieu);
	docFile(argv[2], chuoiDeTim);
	if (chuoiDeTim.empty() || duLieu.empty()) {
		ghiFile(argv[3], "", duLieu);
		system("pause");
		return -1;
	}
	ghiFile(argv[3], chuoiDeTim[0], duLieu);
	system("pause");
	return 0;
}
