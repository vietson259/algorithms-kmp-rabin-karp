#include "docGhiFile.h"
#include "seach.h"

void docFile(string tenFile, vector<string>& duLieu) {
	ifstream docFile;
	duLieu.resize(0);
	docFile.open(tenFile);
	char temp[10000];
	while (docFile.getline(temp, 9999)) {
		duLieu.push_back(temp);
	}
	docFile.close();
}

void ghiFileVector(ofstream& ghi, vector<int>& const data) {
	if (data.size() == 0)
		return;
	if (data.size() == 1)
		ghi << data[0];
	if (data.size() > 1) {
		ghi << data[0];
		for (int i = 1; i < data.size(); i++) {
			ghi << " " << data[i];
		}
	}
}

void ghiFile(string tenFile, string chuoiDeTim, vector<string> duLieu) {
	bool flag = true;
	int pos, k;
	string ketQua;
	ofstream ghiFile;
	ghiFile.open(tenFile);
	/*Rabin - Karp
		1. RABIN: true, pos = xx, k
		2. EXAMPLE : true, pos = xx, k
		3. TEST : true, pos = xx, k
		4. STRINGS : fail*/
	ghiFile << "Rabin - Karp\n";
	for (int i = 0; i < duLieu.size(); i++) {
		flag = seachRBK(chuoiDeTim, duLieu[i], pos, k);
		if (flag) {
			ketQua = "true";
			ghiFile << i + 1 << ". " << duLieu[i] << ": " << ketQua << ", pos = "
				<< pos << ", " << k << "\n";
		}
		else {
			ketQua = "false";
			ghiFile << i + 1 << ". " << duLieu[i] << ": " << ketQua << "\n";
		}
	}
	ghiFile << "\n";
	/*KMP
		1. RABIN: true, pos = xx
		+ NEXT : -1 x x x x
		2. EXAMPLE : true, pos = xx
		+ NEXT : -1 x x x x x x
		3. TEST : true, pos = xx
		+ NEXT : -1 x x x
		4. STRINGS : fail
		+ NEXT : -1 x x x x x x*/
	ghiFile << "KMP\n";
	vector<int> NEXT;
	for (int i = 0; i < duLieu.size(); i++) {
		initNextKMP(duLieu[i], NEXT);
		flag = seachKMP(chuoiDeTim, duLieu[i], pos);
		if (flag) {
			ketQua = "true";
			ghiFile << i + 1 << ". " << duLieu[i] << ": " << ketQua << ", pos = " << pos << "\n"; 
		}
		else {
			ketQua = "false";
			ghiFile << i + 1 << ". " << duLieu[i] << ": " << ketQua << "\n";
		}
		ghiFile << "+ NEXT: ";
		ghiFileVector(ghiFile, NEXT);
		if (i < duLieu.size() - 1)
			ghiFile << "\n";
	}
	ghiFile.close();
}